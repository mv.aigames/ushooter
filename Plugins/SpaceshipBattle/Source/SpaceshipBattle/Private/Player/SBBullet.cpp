// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/SBBullet.h"
#include "Engine/DamageEvents.h"

#include "Components/SphereComponent.h"

// Sets default values
ASBBullet::ASBBullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Lifetime = 2.0;
	Speed = 3000.0;
	damage = 10.0f;

	sphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Capsule"));
	RootComponent = sphereComponent;

	FBodyInstance* body = sphereComponent->GetBodyInstance();
	body->SetEnableGravity(false);
	body->SetPhysicsDisabled(false);
	body->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

}

// Called when the game starts or when spawned
void ASBBullet::BeginPlay()
{
	Super::BeginPlay();
	startTime = GetWorld()->GetTimeSeconds();
	
}

// Called every frame
void ASBBullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	double currentTime = GetWorld()->GetTimeSeconds();
	if (currentTime - startTime > Lifetime)
	{

		OnLifetimeEnded();
	}
	else
	{
		//UE_LOG(SBLog, Warning, TEXT("direciton: %s"), *GetOwner()->GetActorRotation().Vector().ToString());
		FHitResult hit;
		Direction = GetActorRotation().Vector();
		AddActorWorldOffset(Direction * Speed * DeltaTime, true, &hit);
		if (hit.GetActor())
		{
			FDamageEvent e(damageClass ? damageClass: UDamageType::StaticClass());
			hit.GetActor()->TakeDamage(damage, e, nullptr, GetOwner());
			Clean();
		}
	}

}

void ASBBullet::SetBulletDirection(const FVector& BulletDirection)
{
	Direction = BulletDirection;
	UE_LOG(SBLog, Warning, TEXT("direciton: %s"), *Direction.ToString());
}

void ASBBullet::OnLifetimeEnded()
{
	Clean();
}

void ASBBullet::Clean()
{
	Destroy();
	SetActorTickEnabled(false);
}

