// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/SBSpaceshipMovementComponent.h"

#include "Player/SBSpaceship.h"
#include "SBLogs.h"

USBSpaceshipMovementComponent::USBSpaceshipMovementComponent()
{
	DefaultImpulse = 500.0f;
	SlantSpeed = 30.0f;
	MaxSlant = 45.0f;
	Roll = 0.0f;
	Pitch = 0.0f;
	OrientationChangeSpeed = 30.0f;
}

void USBSpaceshipMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	UpdateOrientation(DeltaTime); 
	ConsumeInputVector();
}

void USBSpaceshipMovementComponent::UpdateOrientation(const float DeltaTime)
{
	ASBSpaceship* Ship = GetSpaceship();
	FVector InputVelocity = Ship->GetLastMovementInputVector();

	//UE_LOG(SBLog, Warning , TEXT("PawnOwner: %s"), PawnOwner.Get() ? *PawnOwner->GetName() : TEXT("Null"));
	//UE_LOG(SBLog, Log, TEXT("InputMovement: %s"), *InputVelocity.ToString())

	if (InputVelocity.X != 0.0)
	{
		Roll = FMath::Clamp(Roll + SlantSpeed * DeltaTime * FMath::Sign(InputVelocity.X), -MaxSlant, MaxSlant);
	}
	else
	{
		Roll = Roll * 0.9f;
	}


	if (InputVelocity.Y != 0.0)
	{
		Pitch = FMath::Clamp(Pitch + SlantSpeed * DeltaTime * FMath::Sign(InputVelocity.Y), -MaxSlant, MaxSlant);
	}
	else
	{
		Pitch = Pitch * 0.9f;
	}


	FRotator r(0.0, OrientationChangeSpeed * DeltaTime * (Roll / MaxSlant), 0.0);
	Ship->AddActorWorldRotation(r);

	Velocity = Ship->GetActorForwardVector() * DefaultImpulse;
	FHitResult hit;
	MoveUpdatedComponent(Velocity, Ship->GetActorRotation(), true, &hit);

	if (hit.GetActor())
	{
		UE_LOG(SBLog, Log, TEXT("'%s' has Collided with '%s'"), *GetName(), *hit.GetActor()->GetName());
	}

	//UE_LOG(SBLog, Log, TEXT("Roll: %f.... Pitch: %f"), Roll, Pitch);
}
