// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/SBSpaceship.h"

#include "Components/StaticMeshComponent.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"

#include "Player/SBWeapon.h"
#include "Player/SBSpaceshipMovementComponent.h"
#include "SpaceObjects/SBWorldLimits.h"
#include "SBLogs.h"

// Sets default values
ASBSpaceship::ASBSpaceship()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	sceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	RootComponent = sceneComponent;
	//SetRootComponent(sceneComponent);

	bodyPivotComponent = CreateDefaultSubobject<USceneComponent>(TEXT("BodyPivot"));
	bodyPivotComponent->SetupAttachment(sceneComponent);

	meshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BodyMesh"));
	meshComponent->SetupAttachment(bodyPivotComponent);

	FBodyInstance* body = meshComponent->GetBodyInstance();
	body->SetPhysicsDisabled(false);
	body->SetEnableGravity(true);
	body->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

	MovementComponent = CreateDefaultSubobject<USBSpaceshipMovementComponent>(TEXT("MovementComponent"));
	MovementComponent->SetSpaceship(this);
	MovementComponent->UpdatedComponent = sceneComponent;
}

// Called when the game starts or when spawned
void ASBSpaceship::BeginPlay()
{
	Super::BeginPlay();

	GetComponents<USBWeapon>(weapons);
	if (weapons.Num() == 0)
	{
		UE_LOG(SBLog, Log, TEXT("This spaceship has no weapon"));
	}

	ASBWorldLimits* WorldLimits = Cast<ASBWorldLimits>(UGameplayStatics::GetActorOfClass(GetWorld(), ASBWorldLimits::StaticClass()));
	if (WorldLimits)
	{
		UE_LOG(SBLog, Warning, TEXT("REgistering Spaceship tracking"));
		WorldLimits->AddTrackable(this);
	}
}

// Called every frame
void ASBSpaceship::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (bWantShoot)
	{
		for (USBWeapon* weapon : weapons)
		{
			weapon->Shoot();
		}
	}

	FRotator CurrentAiming = MovementComponent->GetCurrentRotator();
	//UE_LOG(SBLog, Log, TEXT("Rotating to: %s"), *CurrentAiming.ToString());
	meshComponent->SetRelativeRotation(CurrentAiming);

//if (Roll == Aiming.Roll && Aiming.Roll != 0.0)
//{
//	FRotator r(0.0, OrientationChangeSpeed * DeltaTime * FMath::Sign(Roll), 0.0);
//	GetOwner()->AddActorWorldRotation(r);
//	UE_LOG(SBLog, Log, TEXT("Rotating ship...."));
//}
	//UpdateOrientation(DeltaTime);
}

// Called to bind functionality to input
void ASBSpaceship::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}



void ASBSpaceship::Move(const FVector2D& pDirection)
{
	//Aiming = FRotator(pDirection.Y * MaxSlant, 0.0, pDirection.X * MaxSlant);

	//UE_LOG(SBLog, Log, TEXT("Direciton: %s"), *pDirection.ToString());
	AddMovementInput(FVector(pDirection.X, pDirection.Y, 0.0));
	
	
	/*if (Aiming.Vector() == FVector::ForwardVector)
	{
		OldAiming = FQuat(meshComponent->GetRelativeRotation());
	}*/
}

UPawnMovementComponent* ASBSpaceship::GetMovementComponent() const
{
	return MovementComponent;
}

void ASBSpaceship::Impulse(bool pValue)
{
}

void ASBSpaceship::Shoot(bool pValue)
{
	bWantShoot = pValue;
}

EOutOfBoundAction ASBSpaceship::NotifyOutOfBounds()
{
	UE_LOG(SBLog, Warning, TEXT("Spaceship is out of bounds"));
	SetActorLocation(FVector::ZeroVector);
	return EOutOfBoundAction::Nothing;
}