// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/SBWeapon.h"

#include "Player/SBBullet.h"
#include "SBLogs.h"

// Sets default values for this component's properties
USBWeapon::USBWeapon()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	latency = 1.0f;
	lastShootTime = 0.0f;
	//bWantToShoot = false;
	// ...
}


// Called when the game starts
void USBWeapon::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void USBWeapon::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	
}

void USBWeapon::ShootImpl()
{
	if (!bulletClass) { return; }

	FActorSpawnParameters param;
	FVector l = GetComponentLocation();
	FRotator r = GetComponentRotation();
	/*UE_LOG(SBLog, Warning, TEXT("Buller direciton: %s"), *r.Vector().ToString());*/
	ASBBullet* bullet = Cast<ASBBullet>(GetWorld()->SpawnActor(bulletClass, &l, &r));
	bullet->SetActorLocation(l);
	//bullet->SetBulletDirection(r.Vector());
	bullet->SetOwner(GetOwner());
	//bullet->GetRootComponent()->ComponentVelocity
}

void USBWeapon::Shoot()
{
	double gameTime = GetWorld()->GetTimeSeconds();
	if (gameTime - lastShootTime > latency)
	{
		ShootImpl();
		lastShootTime = gameTime;
	}
}

