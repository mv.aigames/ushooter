// Fill out your copyright notice in the Description page of Project Settings.


#include "GameModes/WorldGameMode.h"

#include "Engine/World.h"

#include "Player/SBSpaceship.h"
#include "SpaceObjects/SBWorldLimits.h"
#include "Controllers/SBSpaceshipPlayerController.h"
#include "SBLogs.h"

AWorldGameMode::AWorldGameMode()
{
	DefaultPawnClass = ASBSpaceship::StaticClass();
	PlayerControllerClass = ASBSpaceshipPlayerController::StaticClass();
}

void AWorldGameMode::BeginPlay()
{
	Super::BeginPlay();

	UClass* WorldLimitsClassToUse = WorldLimitsClass ? WorldLimitsClass : ASBWorldLimits::StaticClass();
	WorldLimits = GetWorld()->SpawnActor<ASBWorldLimits>(WorldLimitsClassToUse);

	if (WorldLimits)
	{
		WorldLimits->SetActorLocation(FVector::ZeroVector);
	}
}
