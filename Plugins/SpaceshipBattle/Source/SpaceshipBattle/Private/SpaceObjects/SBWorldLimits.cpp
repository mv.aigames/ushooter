// Fill out your copyright notice in the Description page of Project Settings.


#include "SpaceObjects/SBWorldLimits.h"


#include "SBLogs.h"

// Sets default values
ASBWorldLimits::ASBWorldLimits()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = SceneRoot;

#ifdef WITH_EDITOR
	GridPivot = CreateDefaultSubobject<USceneComponent>(TEXT("GridPivot"));
	GridPivot->SetupAttachment(SceneRoot);
#endif

}



// Called when the game starts or when spawned
void ASBWorldLimits::BeginPlay()
{
	Super::BeginPlay();
	
#ifdef WITH_EDITOR
	double Meter = 100.0;
	FVector From = FVector(-Meter * 1000.0, -Meter * 1000.0, 0.0);
	FVector To = -From;
	int32 Step = 100 * Meter;

	if (!GridMesh)
	{
		UE_LOG(SBLog, Error, TEXT("A mesh for the Grid must be provided"));
		return;
	}

	UE_LOG(SBLog, Log, TEXT("From: %s"), *From.ToString());
	UE_LOG(SBLog, Log, TEXT("To: %s"), *To.ToString());

	//TFunction<void()> HideShowFunc;
	//if (bShowGridDebug)
	//{
	//	HideShowFunc = [] {}
	//}

	GridPivot->SetVisibility(bShowGridDebug);
	for (int32 Row = (int32)From.X; Row < (int32)To.X; Row += Step)
	{
		for (int32 Col = (int32)From.Y; Col < (int32)To.Y; Col += Step)
		{
			UStaticMeshComponent* MeshComponent = NewObject<UStaticMeshComponent>(this);
			FAttachmentTransformRules Rules(EAttachmentRule::KeepRelative, false);
			MeshComponent->AttachToComponent(GridPivot, Rules);
			MeshComponent->RegisterComponent();
			MeshComponent->SetStaticMesh(GridMesh);
			MeshComponent->SetRelativeLocation(FVector(Row, Col, 0.0));
			UE_LOG(SBLog, Log, TEXT("Adding cell..."));
		}
	}
#endif
}

void ASBWorldLimits::AddTrackable(ISBSpaceObjectTrackable* TrackingActor)
{
	TrackingObjects.Add(TrackingActor);

	//FSpaceObjectOutOfBoundsSiganture* OOB = TrackingActor->GetOutOfBoundSignature();
	//TDelegate<void(ISBSpaceObjectTrackable*)> Delegate = FSpaceObjectOutOfBoundsSiganture::FDelegate::CreateUObject(this, &ASBWorldLimits::OnOutOfBounds);
	//OOB->Add(Delegate);
}

// Called every frame
void ASBWorldLimits::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

#ifdef WITH_EDITOR
	GridPivot->SetVisibility(bShowGridDebug);
#endif

	UpdateTrackingData();
}

#ifdef WITH_EDITOR

void ASBWorldLimits::SetShowGrid(bool Value)
{
}

#endif

void ASBWorldLimits::OnOutOfBounds(ISBSpaceObjectTrackable* SpaceObject)
{
	EOutOfBoundAction Action = SpaceObject->NotifyOutOfBounds();

	if (Action == EOutOfBoundAction::Nothing)
	{
		return;
	}

	if (Action == EOutOfBoundAction::StopTracking)
	{
	}
}

void ASBWorldLimits::UpdateTrackingData()
{
	TArray<ISBSpaceObjectTrackable*> ToRemove;
	for (ISBSpaceObjectTrackable* Object : TrackingObjects)
	{
		double DistanceSquared = Cast<AActor>(Object)->GetActorLocation().SizeSquared();
		if (DistanceSquared > MaxRadius * MaxRadius)
		{
			EOutOfBoundAction Action = Object->NotifyOutOfBounds();

			if (Action == EOutOfBoundAction::StopTracking)
			{
				ToRemove.Add(Object);
			}
		}
	}

	for (ISBSpaceObjectTrackable* Object : ToRemove)
	{
		TrackingObjects.Remove(Object);
	}
}