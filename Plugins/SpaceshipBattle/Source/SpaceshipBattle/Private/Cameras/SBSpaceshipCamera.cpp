// Fill out your copyright notice in the Description page of Project Settings.


#include "Cameras/SBSpaceshipCamera.h"

#include "Camera/CameraComponent.h"
#include "engine/World.h"

// Sets default values
ASBSpaceshipCamera::ASBSpaceshipCamera()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
	
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	RootComponent = CameraComponent;
	MainCamera = false;
	Target = nullptr;
	Offset = 1000.0;
	Height = 300.0;
}

// Called when the game starts or when spawned
void ASBSpaceshipCamera::BeginPlay()
{
	Super::BeginPlay();

	if (MainCamera)
	{
		ASBSpaceshipCamera* Camera =  Cast<ASBSpaceshipCamera>(GetWorld()->GetFirstPlayerController()->GetViewTarget());
		if (Camera)
		{
			Camera->MainCamera = false;
		}
		GetWorld()->GetFirstPlayerController()->SetViewTarget(this);
	}
}

// Called every frame
void ASBSpaceshipCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SetActorLocation(Target->GetActorLocation() - GetActorForwardVector() * Offset + FVector::UpVector * Height);

	UpdateRotation(DeltaTime);
	//CameraComponent->SetRelativeRotation();
	
}

void ASBSpaceshipCamera::UpdateRotation(const float DeltaTime)
{
	SetActorRotation(Target->GetActorRotation());
}

void ASBSpaceshipCamera::SetTarget(AActor* FollowTarget)
{
	Target = FollowTarget;
	SetActorTickEnabled(Target != nullptr);
}

