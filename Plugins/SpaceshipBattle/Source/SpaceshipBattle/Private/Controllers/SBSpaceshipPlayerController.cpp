// Fill out your copyright notice in the Description page of Project Settings.


#include "Controllers/SBSpaceshipPlayerController.h"

#include "InputMappingContext.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"

#include "Player/SBSpaceship.h"
#include "Cameras/SBSpaceshipCamera.h"
#include "Controllers/SBSpaceshipInputActions.h"
#include "SBLogs.h"


ASBSpaceshipPlayerController::ASBSpaceshipPlayerController()
{
	contextMapping = nullptr;
	Offset = 100.0;
	LocalCamera = nullptr;
	LocalCameraClass = ASBSpaceshipCamera::StaticClass();
}

void ASBSpaceshipPlayerController::BeginPlay()
{
	if (!LocalCameraClass)
	{
		UE_LOG(SBLog, Warning, TEXT("SpaceshipCamera Class has not been assigned"));
		return;
	}

	LocalCamera = GetWorld()->SpawnActor<ASBSpaceshipCamera>(LocalCameraClass);
	LocalCamera->SetTarget(Spaceship);
	//UE_LOG(SBLog, Log, TEXT("ASBSpaceshipPlayerController::BeginPlay"));
}

void ASBSpaceshipPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	
}

void ASBSpaceshipPlayerController::OnPossess(APawn* pPawn)
{
	Super::OnPossess(pPawn);

	Spaceship = Cast<ASBSpaceship>(pPawn);

	if (!Spaceship)
	{
		UE_LOG(SBLog, Error, TEXT("This SBPlayerController requires an ASBSpaceship pawn"));
		SetActorTickEnabled(false);
		return;
	}

	//UE_LOG(SBLog, Log, TEXT("ASBSpaceshipPlayerController::OnPossess"));

	/*if (LocalCamera)
	{
		LocalCamera->SetTarget(Spaceship);
	}*/
}

void ASBSpaceshipPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	UEnhancedInputLocalPlayerSubsystem* subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer());

	if (!subsystem)
	{
		UE_LOG(SBLog, Error, TEXT("This player controller depends on Enhanced input system"));
		return;
	}

	if (!contextMapping)
	{
		UE_LOG(SBLog, Error, TEXT("A Mapping Context must be provided"));
		return;
	}

	UEnhancedInputComponent* EnhancedInput = Cast<UEnhancedInputComponent>(InputComponent);
	if (!EnhancedInput)
	{
		UE_LOG(SBLog, Error, TEXT("Failed to get 'UEnhancedInputComponent' from InputComponent"));
		return;
	}

	if (!EnhancedShoot)
	{
		UE_LOG(SBLog, Error, TEXT("EnhancedShoot must be assigned"));
		return;
	}

	if (!EnhancedMove)
	{
		UE_LOG(SBLog, Error, TEXT("EnhancedShoot must be assigned"));
		return;
	}

	subsystem->ClearAllMappings();
	subsystem->AddMappingContext(contextMapping, 1);

	// Now bind everything.

	EnhancedInput->BindAction(EnhancedShoot, ETriggerEvent::Started, this, &ASBSpaceshipPlayerController::OnShoot);
	EnhancedInput->BindAction(EnhancedShoot, ETriggerEvent::Completed, this, &ASBSpaceshipPlayerController::OnShoot);
	EnhancedInput->BindAction(EnhancedMove, ETriggerEvent::Triggered, this, &ASBSpaceshipPlayerController::OnMove);
	EnhancedInput->BindAction(EnhancedMove, ETriggerEvent::Completed, this, &ASBSpaceshipPlayerController::OnMove);
}

void ASBSpaceshipPlayerController::OnShoot(const FInputActionValue& ActionValue)
{
	if (Spaceship)
	{
		Spaceship->Shoot(ActionValue.Get<bool>());
	}
	//UE_LOG(SBLog, Log, TEXT("Shooting: %d"), ActionValue.Get<bool> () ? 1 : 0);
}

void ASBSpaceshipPlayerController::OnMove(const FInputActionValue& ActionValue)
{
	if (Spaceship)
	{
		//UE_LOG(SBLog, Log, TEXT("Moving: %s"), *ActionValue.Get<FInputActionValue::Axis2D>().ToString());
		Spaceship->Move(ActionValue.Get<FInputActionValue::Axis2D>());
	}
	
}