// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SBSpaceshipCamera.generated.h"

UCLASS()
class SPACESHIPBATTLE_API ASBSpaceshipCamera : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASBSpaceshipCamera();

	UPROPERTY(EditAnywhere, Category = Camera)
	class UCameraComponent* CameraComponent;

	UPROPERTY(EditAnywhere, Category = Camera)
		bool MainCamera;

	UPROPERTY(VisibleAnywhere, Category = Target)
		AActor* Target;

	UPROPERTY(EditAnywhere, Category = Target, meta = (ClampMin = 0, UIMin = 0, Units = "cm"))
		double Offset;

	UPROPERTY(EditAnywhere, Category = Target, meta = (ClampMin = 0, UIMin = 0, Units = "cm"))
		double Height;
private:
	void UpdateRotation(const float DeltaTime);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void SetTarget(AActor* FollowTarget);

};
