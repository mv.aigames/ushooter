// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "SBSpaceObjectTrackable.generated.h"

class ISBSpaceObjectTrackable;

DECLARE_MULTICAST_DELEGATE_OneParam(FSpaceObjectOutOfBoundsSiganture, ISBSpaceObjectTrackable*);

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class USBSpaceObjectTrackable : public UInterface
{
	GENERATED_BODY()
};

UENUM()
enum class EOutOfBoundAction : uint8
{
	StopTracking,
	Nothing,
};

/**
 * 
 */
class SPACESHIPBATTLE_API ISBSpaceObjectTrackable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual FSpaceObjectOutOfBoundsSiganture* GetOutOfBoundSignature() { return nullptr; }
	virtual EOutOfBoundAction NotifyOutOfBounds() { return EOutOfBoundAction::Nothing; }
	//FSpaceObjectOutOfBoundsSiganture* GetOutOfBoundSignature_Implementation() { return nullptr; }
};
