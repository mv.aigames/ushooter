// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "SBSpaceshipMovementComponent.generated.h"

class ASBSpaceship;
/**
 * 
 */
UCLASS()
class SPACESHIPBATTLE_API USBSpaceshipMovementComponent : public UPawnMovementComponent
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = Movement, meta = (ClampMin = 0, UIMin = 0, ForceUnits = "cm/s"))
		float DefaultImpulse;

	UPROPERTY(EditAnywhere, Category = Movement, meta = (ClampMin = 0, UIMin = 0, ClampMax = 80, UIMax = 80, Units = "Degrees"))
		float MaxSlant;

	UPROPERTY(EditAnywhere, Category = Movement, meta = (ClampMin = 0, UIMin = 0))
		float SlantSpeed;

	UPROPERTY(VisibleAnywhere, Category = Movement)
		float Roll;

	UPROPERTY(VisibleAnywhere, Category = Movement)
		float Pitch;

	UPROPERTY(VisibleAnywhere, Category = Movement)
		float OrientationChangeSpeed;

	UPROPERTY(VisibleAnywhere, Category = Movement)
		FRotator Aiming;

	UPROPERTY(VisibleAnywhere, Category = Movement)
		FQuat OldAiming;

	UPROPERTY(VisibleAnywhere, Category = Movement)
		float AimingLerpValue;

	ASBSpaceship* Spaceship;

public:
	USBSpaceshipMovementComponent();
	void SetSpaceship(ASBSpaceship* InSpaceship);
	ASBSpaceship* GetSpaceship();

	float GetRoll() const;
	float GetPitch() const;
	FRotator GetCurrentRotator() const;

protected:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	void UpdateOrientation(const float DeltaTime);
};


void USBSpaceshipMovementComponent::SetSpaceship(ASBSpaceship* InSpaceship)
{
	Spaceship = InSpaceship;
}

ASBSpaceship* USBSpaceshipMovementComponent::GetSpaceship()
{
	return Spaceship;
}

float USBSpaceshipMovementComponent::GetRoll() const
{
	return Roll;
}

float USBSpaceshipMovementComponent::GetPitch() const
{
	return Pitch;
}

FRotator USBSpaceshipMovementComponent::GetCurrentRotator() const
{
	return FRotator(Pitch, 0.0, Roll);
}