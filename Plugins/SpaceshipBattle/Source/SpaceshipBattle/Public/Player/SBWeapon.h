// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "SBWeapon.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SPACESHIPBATTLE_API USBWeapon : public USceneComponent
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = Weapon)
		double latency;

	UPROPERTY(VisibleAnywhere, Category = Weapon)
		double lastShootTime;

	//UPROPERTY(VisibleAnywhere, Category = Weapon)
	//	bool bWantToShoot;

	UPROPERTY(EditDefaultsOnly, Category = Bullet)
		TSubclassOf<class ASBBullet> bulletClass;
public:	
	// Sets default values for this component's properties
	USBWeapon();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


public:
	void Shoot();
private:
	void ShootImpl();
};
