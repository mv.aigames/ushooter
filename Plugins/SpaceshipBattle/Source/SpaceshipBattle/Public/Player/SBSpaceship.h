// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "Interfaces/SBSpaceObjectTrackable.h"

#include "SBSpaceship.generated.h"

UCLASS()
class SPACESHIPBATTLE_API ASBSpaceship : public APawn, public ISBSpaceObjectTrackable
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, Category = Spaceship)
		bool bWantShoot;

	UPROPERTY(VisibleAnywhere, Category = Spaceship)
		bool bWanttoImpulse;

	UPROPERTY(VisibleAnywhere, Category = Spaceship)
		FVector Direction;

	UPROPERTY(VisibleAnywhere, Category = Weapon)
		TArray<class USBWeapon*> weapons;

	UPROPERTY(EditAnywhere)
		class USceneComponent* sceneComponent;

	UPROPERTY(EditAnywhere)
		class USceneComponent* bodyPivotComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* meshComponent;

	UPROPERTY(EditAnywhere)
		class USBSpaceshipMovementComponent* MovementComponent;


public:
	// Sets default values for this pawn's properties
	ASBSpaceship();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	void Move(const FVector2D& pDirection);
	void Impulse(bool pValue);
	void Shoot(bool pValue);
	UPawnMovementComponent* GetMovementComponent() const override;


public:
	FSpaceObjectOutOfBoundsSiganture OnOutOfBounds;
	virtual FSpaceObjectOutOfBoundsSiganture* GetOutOfBoundSignature() override;
	virtual EOutOfBoundAction NotifyOutOfBounds() override;
};

FSpaceObjectOutOfBoundsSiganture* ASBSpaceship::GetOutOfBoundSignature()
{
	return &OnOutOfBounds;
}

