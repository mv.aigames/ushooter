// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SBBullet.generated.h"

UCLASS()
class SPACESHIPBATTLE_API ASBBullet : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere)
		class USphereComponent* sphereComponent;
	
public:	
	// Sets default values for this actor's properties
	ASBBullet();

	UPROPERTY(EditAnywhere, Category = Lifetime)
		double Lifetime;

	UPROPERTY(EditAnywhere, Category = Movement)
		double Speed;

	UPROPERTY(VisibleAnywhere, Category = Movement)
		FVector Direction;

	UPROPERTY(EditAnywhere, Category = Lifetime)
		float damage;

	UPROPERTY(VisibleAnywhere, Category = Lifetime)
		double startTime;

	UPROPERTY(EditDefaultsOnly, Category = Damage)
		TSubclassOf<UDamageType> damageClass;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	void SetBulletDirection(const FVector& BulletDirection);
	void OnLifetimeEnded();
	void Clean();

};
