// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "SBSpaceshipPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SPACESHIPBATTLE_API ASBSpaceshipPlayerController : public APlayerController
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = Input)
		class UInputMappingContext* contextMapping;

	UPROPERTY(EditDefaultsOnly, Category = Input)
		class UInputAction* EnhancedShoot;

	UPROPERTY(EditDefaultsOnly, Category = Input)
		class UInputAction* EnhancedMove;

	UPROPERTY(VisibleAnywhere, Category = Input)
		class ASBSpaceship* Spaceship;

	UPROPERTY(EditAnywhere, Category = Input)
		double Offset;

	UPROPERTY(EditDefaultsOnly, Category = Camera)
		TSubclassOf<class ASBSpaceshipCamera> LocalCameraClass;

	UPROPERTY(VisibleAnywhere, Category = Camera)
		class ASBSpaceshipCamera* LocalCamera;
	
public:
	ASBSpaceshipPlayerController();
protected:
	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* pPawn) override;
	virtual void SetupInputComponent() override;
	virtual void Tick(float DeltaTime) override;
	//virtual void PawnClientRestart() override;
private:
	void OnShoot(const struct FInputActionValue& ActionValue);
	void OnMove(const struct FInputActionValue& ActionValue);
};
