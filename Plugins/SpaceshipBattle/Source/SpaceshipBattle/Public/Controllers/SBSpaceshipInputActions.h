// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputAction.h"
#include "SBSpaceshipInputActions.generated.h"

class UInputAction;
/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class SPACESHIPBATTLE_API USBSpaceshipInputActions : public UDataAsset
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UInputAction* ShootAction;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UInputAction* MoveAction;
};
