// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "WorldGameMode.generated.h"

/**
 * 
 */
UCLASS()
class SPACESHIPBATTLE_API AWorldGameMode : public AGameModeBase
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = World)
		TSubclassOf<class ASBWorldLimits> WorldLimitsClass;

	TObjectPtr<class ASBWorldLimits> WorldLimits;
public:
	AWorldGameMode();

protected:
	virtual void BeginPlay() override;
};
