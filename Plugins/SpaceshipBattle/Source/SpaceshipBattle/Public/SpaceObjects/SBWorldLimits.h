// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Interfaces/SBSpaceObjectTrackable.h"

#include "SBWorldLimits.generated.h"

UCLASS()
class SPACESHIPBATTLE_API ASBWorldLimits : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere)
	USceneComponent* SceneRoot;

	UPROPERTY(EditAnywhere, Category = Limits, meta = (ClampMin = 0, UIMin = 0))
		float MaxRadius;


//#ifdef WITH_EDITORONLY_DATA
	UPROPERTY(EditAnywhere, Category = Debug)
		class UStaticMesh* GridMesh;
	USceneComponent* GridPivot;
	UPROPERTY(EditAnywhere, Category = Debug)
		bool bShowGridDebug;
	
	void SetShowGrid(bool Value);



//#endif




protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Sets default values for this actor's properties
	ASBWorldLimits();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
public:
	void AddTrackable(ISBSpaceObjectTrackable* TrackingActor);



private:
	TArray<ISBSpaceObjectTrackable*> TrackingObjects;
private:
	void OnOutOfBounds(ISBSpaceObjectTrackable* SpaceObject);
	void UpdateTrackingData();
};


