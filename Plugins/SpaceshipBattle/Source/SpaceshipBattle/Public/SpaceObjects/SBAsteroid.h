// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SBAsteroid.generated.h"

UCLASS()
class SPACESHIPBATTLE_API ASBAsteroid : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = Piece, meta = (ClampMin = 0, UIMin = 0))
		int32 pieces;

	UPROPERTY(EditAnywhere)
		class UStaticMeshComponent* MeshComponent;
	
public:	
	// Sets default values for this actor's properties
	ASBAsteroid();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
